import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api/api.service";
import {Request} from "../models/request.model";
import {User} from "../models/user.model";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-favori',
  templateUrl: './favori.component.html',
  styleUrls: ['./favori.component.css']
})
export class FavoriComponent implements OnInit {

  loggedUser: string = '';
  submitted = false;

  constructor(private apiService: ApiService,
              private authService: AuthService) { }

  ngOnInit() {
    this.getAllFavoriByUser();
  }

  _request:Request[] = [];
  request:Request = new Request()
  favoriId = this.request.id;
  getAllFavoriByUser(){
    this.loggedUser= this.authService.loggedUser
    this.request.name = this.loggedUser;
    this.apiService.getAllFavoriByUserName(this.loggedUser).subscribe(
      (data)=>{this._request=data}
    );

  }
  public result : string = '';

  bingewatch(){
    const random = Math.floor(Math.random() * this._request.length);
    this.submitted = true;
    console.log("Tu regarde : " + this._request[random].name);
    this.result = this._request[random].name;
    console.log("Tu regarde encore : " + this.result);
  }
  //deleteFavori() {
    //this.apiService.deleteFavori(this.favori).subscribe(
      //(data)=>{this.favori=data}
    //);
  //}
}
