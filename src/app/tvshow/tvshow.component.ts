import {Component, Inject, OnInit} from '@angular/core';
import {ApiService} from "../api/api.service";
import {TVShow} from "../models/tvshow.model";
import {ActivatedRoute, Router} from "@angular/router";
import {Request} from "../models/request.model";
import {HomeComponent} from "../home/home.component";
import {formatDate} from "@angular/common";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-tvshow',
  templateUrl: './tvshow.component.html',
  styleUrls: ['./tvshow.component.css']
})
export class TvshowComponent implements OnInit {
  viewMode = false;
  TVShow:TVShow  = new TVShow();
  message = '';
  submitted = false;
  loggedUser: string | undefined = '';


  constructor(private apiService: ApiService,
              private route: ActivatedRoute,
              private router: Router,
              public authService: AuthService) { }

  ngOnInit(): void{
    if (!this.viewMode) {
      this.message = '';
      this.getTVShow(this.route.snapshot.params["id"]);
    }
  }

  getTVShow(id: number): void {
    this.TVShow.first_air_date = new Date();
    formatDate(new Date(), 'dd/MM/yyyy', 'fr');
    this.apiService.getTVShowById(id)
      .subscribe({
        next: (data) => {
          this.TVShow = data[0];
          console.log(data[0]);
        },
        error: (e) => console.error(e)
      });
  }

  addNewFavori(tv:TVShow): void {
    let request: Request = new Request();
    request.id = tv.id;
    request.name = tv.name;
    request.overview = tv.overview;
    request.poster_path = tv.poster_path;
    request.popularity = tv.popularity;
    request.first_air_date = tv.first_air_date;
    console.log(this.TVShow);
    console.log(JSON.stringify(request))
    this.loggedUser = this.authService.loggedUser
    request.userName = this.loggedUser;
    this.apiService.addFavori(request)
      .subscribe({
        next: (data ) => {
          console.log(data);
          this.submitted = true;
        },
        error: (e) => console.error(e)
      });
    alert("La série à bien été ajouté à vos favoris !")
  }





}
