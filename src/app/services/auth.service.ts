import { Injectable } from '@angular/core';
import {User} from "../models/user.model";
import {environment} from "../../environments/environment.prod";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

const API_JAVA_URL = environment.javaApiUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedUser:string = '';
  isloggedIn:boolean = false;
  roles?:string[];

  constructor(private http: HttpClient,
              private route: Router) { }

  //connexion au compte
  searchUserByEmail(email: string) {
    return this.http.get<User>(API_JAVA_URL + '/searchUserMail/' + email);
  }

  logOut() {
    this.isloggedIn = false;
    this.loggedUser = '';
    this.roles = undefined;
    localStorage.removeItem('loggedUser');
    localStorage.setItem('isloggedIn', String(this.isloggedIn));
    this.route.navigate(['/login']);
  }

  signIn(user :User){
    this.loggedUser = user.name;
    this.isloggedIn = true;
    this.roles = user.role;
    localStorage.setItem('loggedUser', <string>this.loggedUser);
    localStorage.setItem('isloggedIn', String(this.isloggedIn));

  }

  //CRUD pour les users
  //création de compte
  addUser(user:User) {
    return this.http.post(API_JAVA_URL + '/adduser' ,user);
  }

  //connexion au compte
  //onLogin(user: string | undefined) {
   // return this.http.get(API_JAVA_URL + '/users' + user);
 // }
}
