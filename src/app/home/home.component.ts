import {Component, Input, OnInit} from '@angular/core';
import {Request} from "../models/request.model";
import {ApiService} from "../api/api.service";
import {TVShow} from "../models/tvshow.model";
import { ActivatedRoute, Router } from '@angular/router';
import {TvshowComponent} from "../tvshow/tvshow.component";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  viewMode = false;
  tvShow:TVShow  = new TVShow();
  currentIndex = -1;
  name = '';
  tvshows:TVShow[] = [];
  message = '';
  loggedUser: string | undefined = '';

  submitted = false;
  constructor(private apiService: ApiService,
              private route: ActivatedRoute,
              private router: Router,
              public authService: AuthService) { }

  ngOnInit(): void {
    if (!this.viewMode) {
      this.message = '';
      this.getTvshowFromApi(this.route.snapshot.params);
    }
    this.getTVShow(this.route.snapshot.params["id"]);
  }

  getTvshowFromApi(param: any){
    this.apiService.getAllTVShow().subscribe(
      (data)=>{this.tvshows=data}
    );
  }

  getTVShow(id: number): void {
    this.apiService.getTVShowById(id)
      .subscribe({
        next: (data) => {
          this.tvShow = data;
          console.log("appel de la data " + data);
        },
        error: (e) => console.error(e)
      });
  }

  searchTVShowByTitle(): void {
      this.tvShow = {
        id : 0
      }
      this.currentIndex = -1;
      this.apiService.findTVShowByTitle(this.name)
        .subscribe({
          next : (data) => {
            this.tvshows = data;
            console.log(data);
          },
          error: (e) => console.error(e)
        });
  }

}
