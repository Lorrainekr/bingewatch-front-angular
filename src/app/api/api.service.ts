import {HttpClient,  HttpHeaders, HttpClientModule} from "@angular/common/http";
import {TVShow} from "../models/tvshow.model";
import {Request} from "../models/request.model";
import {environment} from "../../environments/environment.prod";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {User} from "../models/user.model";
import {AuthService} from "../services/auth.service";

//URL for api
const API_URL = environment.apiUrl;
const API_JAVA_URL = environment.javaApiUrl;


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  loggedUser:string | undefined = '';

  constructor(private http: HttpClient,
              public authService:AuthService) {}

  // CRUD pour l'API mongodb
  getAllTVShow() {
    return this.http.get<TVShow[]>(API_URL + '/TVShowlist');
  }

  getTVShowById(id: any): Observable<any> {
    return this.http.get(API_URL + '/TVShowlist' + '/' + id );
  }

  findTVShowByTitle(name: string): Observable<any> {
    return this.http.get(API_URL + '/TVShowlist' + '/name' + '/' + name);
  }

  findTVShowByYear(first_air_date: string): Observable<any> {
    return this.http.get(API_URL + '/TVShowlist' +  '/first_air_date' + '/' + first_air_date);
  }

  deleteTVShow(id:any): Observable<any> {
    return this.http.delete(API_JAVA_URL + '/TVShowlist' + '/' + id);
  }

  // CRUD pour les favori
  getAllFavoriByUserName(name: string) {
    this.loggedUser = this.authService.loggedUser
    return this.http.get<Request[]>(API_JAVA_URL + '/monespace/' + this.loggedUser);
  }

  //getFavoriByUserId(id: number){
    //return this.http.get<Favori[]>(API_JAVA_URL + '/favoris/' + id);
  //}

  addFavori(request: Request) {
    return this.http.put(API_JAVA_URL + '/addfavorite', request );
  }

  //deleteFavori(favori: Favori) {
     // return this.http.delete(API_JAVA_URL + 'deleteFavorite/');
   // }

}
