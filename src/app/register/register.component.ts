import { Component, OnInit } from '@angular/core';
import {User} from "../models/user.model";
import {ApiService} from "../api/api.service";
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user:User = new User();
  submitted = false;

  constructor(
              private ApiService: ApiService,
              public AuthService: AuthService,
              private router: Router
  ) { }

  ngOnInit(): void {
  }
  createUser(): void {
    const user = {
      name: this.user.name,
      surname: this.user.surname,
      dateMaj: this.user.dateMaj,
      isAdmin: this.user.isAdmin,
      password: this.user.password,
      email: this.user.email
    };
    this.AuthService.addUser(this.user)
      .subscribe({
        next: (res) => {
          console.log("Votre compte a bien été crée !" + res);
          this.submitted = true;
        },
        error: (e) => console.error(e)
      });
    this.router.navigate(['/home']);
  }

}
