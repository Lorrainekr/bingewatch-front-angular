import {Component, NgModule, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../api/api.service";
import {User} from "../models/user.model";
import {AuthService} from "../services/auth.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
// créer une variable connectUser
 conectUser:User = new User();
 user:User = new User();
 submitted = false;
 erreur = 0;

  constructor(
    private ApiService: ApiService,
    private authService: AuthService,
    private router: Router
    ) { }

  ngOnInit(): void {
  }
 /* la méthode finale de connexion */
  onLoggedin() {
    console.log(this.user)
    this.authService.searchUserByEmail(this.user.email).subscribe((user:User) => {
      if (user.email === this.user.email)
      {
        this.authService.signIn(user)
        console.log("vous ètes connecté !")
        this.router.navigate(['/home']);
      }
      else {
        this.erreur = 1;
        console.log("erreur de connexion")
      }
    }, (err) => console.log(err));
  }
}

//alert('Login ou mot de passe incorrecte !')

// méthode avec rest api
// Aller chercher la méthode connexion  de l'api SPRING

// récupérer le résultat de cette méthode
// si tu récup un objet user => ok cnx
// donc Stocker le user dans le local storage

// sinon pop up pour se logger
/*  en vrai cette méthode sera sur la partie register*/
