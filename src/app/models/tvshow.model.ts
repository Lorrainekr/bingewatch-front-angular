export class TVShow {
  id?: any;
  first_air_date?: Date;
  name?: string;
  overview?: string;
  popularity?: number;
  poster_path?: string;
}
