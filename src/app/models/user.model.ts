export class User {
  public id!: number;
  public email! : string;
  public password! : string;
  public name! : string;
  public surname! : string;
  public dateMaj!: Date;
  public isAdmin! : boolean;
  public role?: string[];
}
