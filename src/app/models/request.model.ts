export class Request {
    public userName? : string;
    public id?: number;
    public name?: any;
    public overview?: any;
    first_air_date?: Date | undefined;
    popularity?: number;
    poster_path?: string;

}
