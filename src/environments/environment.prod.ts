export const environment = {
  production: true,

  // url vers l'api heroku
  apiUrl: 'https://backend-bingewatch.herokuapp.com',
  javaApiUrl: 'https://microservice-bingewatch-user.herokuapp.com'
  //javaApiUrl: 'http://localhost:8080'
};
